package de.chagemann.regexcrossword.extensions

import android.content.Context
import android.content.res.Resources
import android.net.ConnectivityManager
import android.util.Log

fun Context.getStringWithResKey(nameResKey: String): String {
    val resId = resources.getIdentifier(nameResKey, "string", this.packageName)
    return try {
        getString(resId)
    } catch (e: Resources.NotFoundException) {
        Log.e(this.javaClass.simpleName, "Couldn't find string value for key '$nameResKey'", e)
        "Resolving string res failed"
    }
}
