package de.chagemann.regexcrossword.db

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
enum class LevelCategory(val nameResKey: String = "level_category_default") {

    @SerializedName("")
    UNKNOWN("level_category_default"),

    @SerializedName("tutorial")
    TUTORIAL("level_category_tutorial"),

    @SerializedName("beginner")
    BEGINNER("level_category_beginner"),

    @SerializedName("intermediate")
    INTERMEDIATE("level_category_intermediate"),

    @SerializedName("advanced")
    ADVANCED("level_category_advanced"),

    @SerializedName("experienced")
    EXPERIENCED("level_category_experienced")
}

