package de.chagemann.regexcrossword.db

import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "crossword_table")
@Keep
data class Crossword(
        val leftInstructions: List<String>,
        val topInstructions: List<String>,
        val levelCategory: LevelCategory,
        @PrimaryKey val name: String,
        val levelCompleted: Boolean = false
)