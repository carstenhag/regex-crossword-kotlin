Solve crosswords by matching regex rules!
These are grouped by difficulty and get increasingly harder to solve.

Will you be able to solve all of them?

- Get to be a regex expert
- Crosswords re-invented
- Free and without any ads

This app is open source: https://gitlab.com/carstenhag/regex-crossword-kotlin

Got some improvement ideas? Either submit an issue on the Gitlab project or send me a mail: regex-crossword@chagemann.de