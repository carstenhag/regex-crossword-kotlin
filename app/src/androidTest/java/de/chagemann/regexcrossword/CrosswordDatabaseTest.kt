package de.chagemann.regexcrossword

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jraska.livedata.test
import de.chagemann.regexcrossword.db.Crossword
import de.chagemann.regexcrossword.db.CrosswordDao
import de.chagemann.regexcrossword.db.CrosswordDatabase
import de.chagemann.regexcrossword.db.LevelCategory
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class CrosswordDatabaseTest {

    @get:Rule
    val testRule = InstantTaskExecutorRule()

    private lateinit var crosswordDao: CrosswordDao
    private lateinit var db: CrosswordDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
                context, CrosswordDatabase::class.java).build()
        crosswordDao = db.crosswordDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun deletingAllCrosswordsWorks() {

        crosswordDao.insert(Crossword(listOf("lala", "lulu"), listOf("bubu", "bibi"), LevelCategory.ADVANCED, "Level 4!"))
        crosswordDao.deleteAll()

        crosswordDao.getCrosswords().test().awaitValue().assertValue{ it.isEmpty() }
    }

    @Test
    @Throws(Exception::class)
    fun writeCrosswordAndReadBack() {

        val crossword = Crossword(listOf("lala", "lulu"), listOf("bubu", "bibi"), LevelCategory.ADVANCED, "Level 4!")
        crosswordDao.insert(crossword)

        val byName = crosswordDao.getCrossword("Level 4!")
        val value = byName.test()
                .awaitValue()
                .assertHasValue()
                .assertValue(crossword)
                .value()
        println(value)
    }

    @Test
    @Throws(Exception::class)
    fun getCrosswordForLevelCategory() {

        crosswordDao.deleteAll()

        crosswordDao.insert(Crossword(listOf("lala", "lulu"), listOf("bubu", "bibi"), LevelCategory.TUTORIAL, "Bad level!"))
        crosswordDao.insert(Crossword(listOf("lala", "lulu"), listOf("bubu", "bibi"), LevelCategory.ADVANCED, "Level 1!"))
        crosswordDao.insert(Crossword(listOf("lala", "lulu"), listOf("bubu", "bibi"), LevelCategory.ADVANCED, "Level 2!"))

        crosswordDao.getCrosswords(LevelCategory.ADVANCED)
                .test()
                .awaitValue()
                .assertHasValue()
                .assertValue{ value -> value.all {
                    it.levelCategory == LevelCategory.ADVANCED
                }}
                .value()
    }
}