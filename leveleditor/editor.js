// SRC: Bootstrap Icons
var trash = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/><path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/></svg>';

var arrow = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-in-up-left" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M9.636 13.5a.5.5 0 0 1-.5.5H2.5A1.5 1.5 0 0 1 1 12.5v-10A1.5 1.5 0 0 1 2.5 1h10A1.5 1.5 0 0 1 14 2.5v6.636a.5.5 0 0 1-1 0V2.5a.5.5 0 0 0-.5-.5h-10a.5.5 0 0 0-.5.5v10a.5.5 0 0 0 .5.5h6.636a.5.5 0 0 1 .5.5z"/><path fill-rule="evenodd" d="M5 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H6.707l8.147 8.146a.5.5 0 0 1-.708.708L6 6.707V10.5a.5.5 0 0 1-1 0v-5z"/></svg>';


// SRC: StackOverflow
if (!String.prototype.splice) {
    /**
     * {JSDoc}
     *
     * The splice() method changes the content of a string by removing a range of
     * characters and/or adding new characters.
     *
     * @this {String}
     * @param {number} start Index at which to start changing the string.
     * @param {number} delCount An integer indicating the number of old chars to remove.
     * @param {string} newSubStr The String that is spliced in.
     * @return {string} A new string with the spliced substring.
     */
    String.prototype.splice = function (start, delCount, newSubStr) {
        return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
    };
}



// Load the JSON
function load() {
    var text = document.getElementById("js").value;
    //try {
    js = JSON.parse(text);
    document.getElementById("js").classList.remove("error");
    if (typeof js.possibleSolution == 'undefined') {
        js.possibleSolution = Array(js.leftInstructions.length);
        for (let q = 0; q < js.possibleSolution.length; q++) {
            js.possibleSolution[q] = Array(js.topInstructions.length).fill("");
        }
    }
    return js;
    //} catch (e) {
    //	document.getElementById("js").classList.add("error");
    //	return "";
    //}
}

//Save to JSON
function save(js) {
    //document.getElementById("js").value = JSON.stringify(js, null, 2);
    var text = JSON.stringify(js);
    text = text.splice(1, 0, "\n\t").splice(-1, 0, "\n").replace('"left', '\n\t"left').replace('"name', '\n\t"name').replace('"poss', '\n\t"poss').replace('"level', '\n\t"level');
    document.getElementById("js").value = text;
}

//Render the values - Attention! Overwrites the current Changes!
function render(maxRow = 0, maxCol = 0) {
    var js = load();
    if (js == "") { return }

    if (maxRow == 0) {
        maxRow = js.leftInstructions.length;
    }
    if (maxCol == 0) {
        maxCol = js.topInstructions.length;
    }

    var main = document.getElementById("main");
    main.classList = "main";
    main.classList.add("grid_" + (maxCol + 1));
    main.innerHTML = "";
    for (var row = 0; row <= maxRow; row++) {
        for (var col = 0; col <= maxCol; col++) {
            var div = document.createElement("div");

            if (row == 0) {
                if (col == 0) {
                    main.appendChild(div);
                    continue;
                }
                //render a Title Row
                div.innerHTML = '<input type="text" id="top_' + col + '" value="' + js.topInstructions[col - 1] + '" oninput="changeTop(this, ' + (col - 1) + ')">';
                div.classList.add("top");
                div.innerHTML += '<font onclick="delTop(' + (col - 1) + ')">' + trash + '</font>';
                div.innerHTML += '<font onclick="addTop(' + (col - 1) + ')">' + arrow + '</font>';
                main.appendChild(div);
                continue;
            }
            if (col == 0) {
                //render a Left Title
                div.innerHTML = '<input type="text" id="left_' + row + '" value="' + js.leftInstructions[row - 1] + '" oninput="changeLeft(this, ' + (row - 1) + ')">';
                div.classList.add("left");
                div.innerHTML += '<font onclick="delLeft(' + (row - 1) + ')">' + trash + '</font>';
                div.innerHTML += '<font onclick="addLeft(' + (row - 1) + ')">' + arrow + '</font>';
                main.appendChild(div);
            } else {
                //render a cell
                var text = "";
                if (row - 1 < js.possibleSolution.length && col - 1 < js.possibleSolution[row - 1].length) {
                    text = js.possibleSolution[row - 1][col - 1];
                }
                div.innerHTML = '<input type="text" value="' + text + '" oninput="changeSol(this, ' + (row - 1) + ', ' + (col - 1) + ')">';
                div.classList.add("cell");
                main.appendChild(div);
            }


        }
    }
    check();
}

// Check the Regex
function check() {
    var js = load();
    for (let col = 0; col < js.topInstructions.length; col++) {
        const element = js.topInstructions[col];
        var regex = new RegExp("^" + element + "$");
        var text = "";
        for (let row = 0; row < js.leftInstructions.length; row++) {
            text += js.possibleSolution[row][col];
        }
        if (regex.test(text)) {
            //success
            document.getElementById('top_' + (col + 1)).classList.remove("error");
        } else {
            //not success
            document.getElementById('top_' + (col + 1)).classList.add("error");
        }
    }
    for (let row = 0; row < js.leftInstructions.length; row++) {
        const element = js.leftInstructions[row];
        var regex = new RegExp("^" + element + "$");
        var text = js.possibleSolution[row].join('');
        if (regex.test(text)) {
            //success
            document.getElementById('left_' + (row + 1)).classList.remove("error");
        } else {
            //not success
            document.getElementById('left_' + (row + 1)).classList.add("error");
        }
    }
    var elems = document.getElementsByClassName("cell");
    for (let i = 0; i < elems.length; i++) {
        e = elems[i];
        if (e.firstChild.value.length != 1) {
            e.firstChild.classList.add("error");
        } else {
            e.firstChild.classList.remove("error");
        }
    }
}

//Change, Add and Delete the Values
function changeTop(e, i) {
    var js = load();
    js.topInstructions[i] = e.value;
    save(js);
    check();
}
function delTop(i) {
    var js = load();
    js.topInstructions.splice(i, 1);
    for (let row = 0; row < js.possibleSolution.length; row++) {
        js.possibleSolution[row].splice(i, 1);
    }
    save(js);
    render();
}
function addTop(i) {
    var js = load();
    js.topInstructions.splice(i, 0, "");
    for (let row = 0; row < js.possibleSolution.length; row++) {
        js.possibleSolution[row].splice(i, 0, "");
    }
    save(js);
    render();
}
function changeLeft(e, i) {
    var js = load();
    js.leftInstructions[i] = e.value;
    save(js);
    check();
}
function delLeft(i) {
    var js = load();
    js.leftInstructions.splice(i, 1);
    js.possibleSolution.splice(i, 1);
    save(js);
    render();
}
function addLeft(i) {
    var js = load();
    var newArray = Array(js.leftInstructions.length);
    for (let q = 0; q < newArray.length; q++) { newArray[q] = ""; }
    js.leftInstructions.splice(i, 0, "");
    js.possibleSolution.splice(i, 0, newArray);
    save(js);
    render();
}
function changeSol(e, row, col) {
    var js = load();
    if (row >= js.possibleSolution.length) {
        js.possibleSolution[row] = [];
    }
    js.possibleSolution[row][col] = e.value;
    save(js);
    check();
}

function removeSol() {
    var js = load();
    for (let row = 0; row < js.possibleSolution.length; row++) {
        for (let col = 0; col < js.possibleSolution[row].length; col++) {
            js.possibleSolution[row][col] = "";
        }
    }
    save(js);
    render();
}


//Start rendering on Page Load
document.addEventListener('DOMContentLoaded', function () {
    render();
}, false);
