# Regex Crossword Kotlin

[![pipeline status](https://gitlab.com/carstenhag/regex-crossword-kotlin/badges/develop/pipeline.svg)](https://gitlab.com/carstenhag/regex-crossword-kotlin/commits/develop)

[Play Store Link](https://play.google.com/store/apps/details?id=de.chagemann.regexcrossword)

## Changelog
2021-03-20: I've only been updating this repo every few months so F-Droid builds it some day so it's included in the F-Droid store :D. Not much more maintenance or other things are done. The architecture definitely should be refactored, but I don't have time for it right now.
2023-08-18: Update libraries, targetSdkVersion, etc in order to stay compliant with the Play Store.
2023-11-24: Added HTML level editor contributed by @dornteufel.

## Level Editor:
At /leveleditor there's a HTMl/JS level editor to make creation & testing of crosswords easier.
It's not shipped with the app, and it's not meant to be used for that.

After creating some levels, please submit a pull request that modifies the crosswords.json file.

## Screenshots

Run `fastlane build_for_screengrab` to build the tests APKs and then `fastlane screengrab` to automatically take screenshots of the app.
